const Player = require("../schemas/playerSchema");
const Team = require("../schemas/teamSchema");
const fetch = require("cross-fetch");

const InitPlayer = async (req, res, next) => {
    const teams = Team.find();
    teams.then(data => {
        data.map(value => {
            const playerurl = "https://www.thesportsdb.com/api/v1/json/50130162/lookup_all_players.php?id="+value.idTeam;

            fetch(playerurl)
                .then(res => {
                    if (res.status >= 400) {
                        throw new Error("bad response from server");
                    }
                    return res.json();
                })
                .then(data => {
                    const { player } = data;

                    for (let i = 0; i < player?.length; i++) {

                        const players = new Player({
                            idPlayer: player[i]?.idPlayer,
                            idTeam: player[i]?.idTeam,
                            idTeam2: player[i]?.idTeam2,
                            idTeamNational: player[i]?.idTeamNational,
                            strNationality: player[i]?.strNationality,
                            strPlayer: player[i]?.strPlayer,
                            strTeam: player[i]?.strTeam,
                            dateBorn: player[i]?.dateBorn,
                            strDescriptionEN: player[i]?.strDescriptionEN,
                            strDescriptionFR: player[i]?.strDescriptionFR,
                            strGender: player[i]?.strGender,
                            strSide: player[i]?.strSide,
                            strSigning: player[i]?.strSigning,
                            strPosition: player[i]?.strPosition,
                            strHeight: player[i]?.strHeight,
                            strWeight: player[i]?.strWeight,
                            strThumb: player[i]?.strThumb
                        });

                        players.save()
                            .then(data => {
                                return res.status(201).json({
                                    message: "player créée avec succès!",
                                    playerDatas: {
                                        ...data,
                                        id: data._id
                                    }
                                });
                            })
                            .catch(error => {
                                console.log(error)
                            });

                    };
                });
        });
    })
};

const getPlayer = async (req, res, next) => {
    const playerQuery = Player.find({idTeam: {$eq: req.params.id}});
    let fetchedPlayers;
    playerQuery.then(documents => {
        fetchedPlayers = documents;
        return Player.count();
    }).then(count => {
        res.status(200).json({
            message: "players affichées avec succès",
            players: fetchedPlayers,
            maxPlayers: count
        });
    })
        .catch(error => {
            res.status(500).json({
                message: "Affichage des données échoués!"
            });
        });
}

exports.InitPlayer = InitPlayer;
exports.getPlayer = getPlayer;