const League = require("../schemas/leagueSchema");
const fetch = require("cross-fetch");

const InitLeague = async (req, res, next) => {
    const leagueUrl = "https://www.thesportsdb.com/api/v1/json/2/all_leagues.php";

    fetch(leagueUrl)
        .then(res => {
            if (res.status >= 400) {
                throw new Error("bad response from server");
            }
            return res.json();
        })
        .then(data => {
            const { leagues } = data;

            for (let i = 0; i < leagues?.length; i++) {

                const league = new League({
                    idLeague: leagues[i]?.idLeague,
                    strLeague: leagues[i]?.strLeague,
                    strSport: leagues[i]?.strSport,
                    strLeagueAlternate: leagues[i]?.strLeagueAlternate
                });

                league.save()
                    .then(data => {
                        return res.status(201).json({
                            message: "league créée avec succès!",
                            leagueDatas: {
                                ...data,
                                id: data._id
                            }
                        });
                    })
                    .catch(error => {
                        console.log(error)
                    });

            };

        });

};

const getLeague = async (req, res, next) => {
    const {search} = req.body;
    
const leagueQuery = League.find({ 'strLeague': {$regex: search, $options: 'i'}});
    let fetchedLeagues;
    leagueQuery.then(documents => {
        fetchedLeagues = documents;
        return League.count();
    }).then(count => {
        res.status(200).json({
            message: "leagues affichées avec succès",
            leagues: fetchedLeagues,
            maxLeagues: count
        });
    })
        .catch(error => {
            res.status(500).json({
                message: error
            });
        });
}

exports.InitLeague = InitLeague;
exports.getLeague = getLeague;