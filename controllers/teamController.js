const Team = require("../schemas/teamSchema");
const League = require("../schemas/leagueSchema");
const fetch = require("cross-fetch");
const HttpError = require('../schemas/http-error');

const InitTeam = async (req, res, next) => {
    const leagues = League.find();
    leagues.then(data => {
        data.map(value => {
            const teamUrl = "https://www.thesportsdb.com/api/v1/json/2/search_all_teams.php?l=" + value.strLeague

            fetch(teamUrl)
                .then(res => {
                    if (res.status >= 400) {
                        throw new Error("bad response from server");
                    }
                    return res.json();
                })
                .then(data => {
                    const { teams } = data;

                    for (let i = 0; i < teams?.length; i++) {

                        const team = new Team({
                            idTeam: teams[i]?.idTeam,
                            strTeam: teams[i]?.strTeam,
                            strTeamShort: teams[i]?.strTeamShort,
                            strTeamBadge: teams[i]?.strTeamBadge,
                            strDescriptionFR: teams[i]?.strDescriptionFR,
                            idLeague: teams[i]?.idLeague
                        });

                        team.save()
                            .then(data => {
                                return res.status(201).json({
                                    message: "team créée avec succès!",
                                    teamDatas: {
                                        ...data,
                                        id: data._id
                                    }
                                });
                            })
                            .catch(error => {
                                console.log(error)
                            });    
                    };
                });

        });
    })


};

const getTeam = async (req, res, next) => {
    const teamQuery = Team.find({idLeague: {$eq: req.params.id}});
    let fetchedTeams;
    teamQuery.then(documents => {
        fetchedTeams = documents;
        return Team.count();
    }).then(count => {
        res.status(200).json({
            message: "teams affichées avec succès",
            teams: fetchedTeams,
            maxTeams: count
        });
    })
        .catch(error => {
            res.status(500).json({
                message: 'Affichage des données échoués!'
            });
        });
}

exports.InitTeam = InitTeam;
exports.getTeam = getTeam;