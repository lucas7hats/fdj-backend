const express = require("express");
const router = express.Router();

const PlayerController = require("../controllers/playerController");

router.get("/init", PlayerController.InitPlayer);
router.get("/:id", PlayerController.getPlayer);

module.exports = router;