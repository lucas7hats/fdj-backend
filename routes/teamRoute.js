const express = require("express");
const router = express.Router();

const TeamController = require("../controllers/teamController");

router.get("/init", TeamController.InitTeam);
router.get("/:id", TeamController.getTeam);

module.exports = router;