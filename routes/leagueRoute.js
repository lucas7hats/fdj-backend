const express = require("express");
const router = express.Router();

const LeagueController = require("../controllers/leagueController");

router.get("/init", LeagueController.InitLeague);
router.post("/list", LeagueController.getLeague);


module.exports = router;