const path = require("path");
const express = require("express");
const bodyParser = require("body-parser");
const mongoose = require("mongoose");

//All route path
const leagueRoute = require("./routes/leagueRoute");
const teamRoute = require("./routes/teamRoute");
const playerRoute = require("./routes/playerRoute");

const dotenv = require('dotenv');

dotenv.config({ path: './config.env' });

const app = express();

mongoose
.connect(
    process.env.DB_URL, {
        useNewUrlParser: true,
       // useUnifiedTopology: true
      }
  )
  .then(() => {
    console.log("Connected to database!");
    console.log("App listen on address " + process.env.HOST +':'+ process.env.PORT );
  })
  .catch((e) => {
    console.log("Connection failed!");
  });

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use((req, res, next) => {
    res.setHeader("Access-Control-Allow-Origin", "*");
    res.setHeader(
      "Access-Control-Allow-Headers",
      "Origin, X-Requested-With, Content-Type, Accept, Authorization"
    );
    res.setHeader(
      "Access-Control-Allow-Methods",
      "GET, POST, PATCH, PUT, DELETE, OPTIONS"
    );
    next();
});

app.use("/api/leagues", leagueRoute);
app.use("/api/teams", teamRoute);
app.use("/api/players", playerRoute);
module.exports = app;
  