const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const teamSchema = new Schema({
    idTeam: {type:String},
    strTeam: {type:String},
    strTeamShort: {type:String},
    strTeamBadge: {type:String},
    strDescriptionFR: {type:String},
    idLeague: {type: String},
    createdAt: {
        type: Date,
        default: Date.now(),
        select: false
    }

});

teamSchema.pre(/^find/, function(next) {
    this.populate({
        path: 'idLeague',
        select: '-_id -createdAt'
    })
    next();
});

module.exports = mongoose.model("Team", teamSchema);