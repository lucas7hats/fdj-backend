const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const leagueSchema = new Schema({
    idLeague: {type:String, required: true, unique: true},
    strLeague: {type:String, required: true},
    strSport: {type:String, required: false},
    strLeagueAlternate: {type:String, required: false},
    createdAt: {
        type: Date,
        default: Date.now(),
        select: false
    }

});

module.exports = mongoose.model("League", leagueSchema);