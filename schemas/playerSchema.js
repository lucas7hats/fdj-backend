const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const playerSchema = new Schema({
          idPlayer:{type:String, required: true},
          idTeam:{type:String, required: true},
          idTeam2:{type:String, required: false},
          idTeamNational:{type:String, required: false},
          strNationality:{type:String, required: false},
          strPlayer:{type:String, required: true},
          strTeam:{type:String, required: false},
          dateBorn:{type:String, required: true},
          strDescriptionEN:{type:String, required: false},
          strDescriptionFR:{type:String, required: false},
          strGender:{type:String, required: false},
          strSide:{type:String, required: false},
          strPosition:{type:String, required: false},
          strSigning: {type: String, require: false},
          strHeight:{type:String, required: false},
          strWeight:{type:String, required: false},
          strThumb:{type:String, required: false}
});

module.exports = mongoose.model("Player", playerSchema);
